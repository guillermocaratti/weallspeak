<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('professionals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->string('name');
            $table->string('email');
            $table->string('phoneNumber')->nullable();
            $table->string('city');
            $table->string('province');
            $table->char('country_id',2);
            $table->string('availableHours')->nullable();
            $table->char('native_language_id',2);
            $table->timestamps();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('native_language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professionals');
    }
}
