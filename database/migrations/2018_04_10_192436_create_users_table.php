<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('email')->unique();
            $table->char('image_hash',12);
            $table->Date('birthDay');
            $table->string('nacionality');
            $table->string('sex');
            $table->string('phoneNumber')->nullable();
            $table->string('city');
            $table->string('province');
            $table->char('country_id',2);
            $table->foreign('country_id')->references('id')->on('countries');
            $table->string('availableHours')->nullable();
            $table->char('native_language_id',2);
            $table->foreign('native_language_id')->references('id')->on('languages');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
