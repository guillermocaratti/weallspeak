<?php

use Faker\Generator as Faker;
use App\Image;
use App\Country;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    $isMale = $faker->boolean;
    $gender1 = $isMale ? "men" : "women";
    $gender2 = $isMale ? "male" : "female";

    $hashImage = substr($faker->unique()->md5,0,12);

    $url = "https://randomuser.me/api/portraits/$gender1/" 
      . rand(1,99)
      . ".jpg";

    $contents = file_get_contents($url);
    $name = substr($url, strrpos($url, '/') + 1);
    Storage::put('public/'.$hashImage.".jpg", $contents);
    
    return [
        'name' => $faker->name($gender2),
        'username' =>  $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'birthDay' => $faker->dateTimeBetween('-40 years', '-18 years'),
        'nacionality' => $faker->country,
        'sex' => $gender2,
        'image_hash' => $hashImage,
        'phoneNumber' => $faker->phoneNumber,
        'city' => $faker->city,
        'province' => $faker->city,
        'country_id' => Country::inRandomOrder()->first()->id,
        'availableHours' => $faker->sentence,
        'native_language_id' => App\Language::inRandomOrder()->first()->id
    ];
});
