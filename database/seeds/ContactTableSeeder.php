<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Contact;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::each( function ($member_1){
          User::where("id","<>",$member_1->id)
                  ->inRandomOrder()
                  ->take(rand(4,6))
                  ->get()
                  ->each(function ($member_2) use ($member_1){
            Contact::create([
              "user_id" => $member_1->id,
              "to_user_id" => $member_2->id,
              "relation" => rand(-1,2)
            ]);
          });
        });
    }
}
