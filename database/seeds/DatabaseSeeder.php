<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
      $this->call(LanguageTableSeeder::class);
      $this->call(CountryTableSeeder::class);
      $this->call(UserTableSeeder::class);
      $this->call(ContactTableSeeder::class);
      //$this->call(ImageTableSeeder::class);
    }
}
