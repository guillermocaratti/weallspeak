@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div>
                  <div class="form-group" >
                     <label>Name</label>
                     <input type="text" id="name" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Last Name</label>
                     <input type="text" id="lname" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Username</label>
                     <input type="text" id="username" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Password</label>
                     <input type="text" id="password" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <labe>Birthday</labe></label>
                     <input type="text" id="bDay" class="form-control" />
                  </div>
                  <div class="form-group" >
                    <label>Sex</label>
                    <input type="text" id="sex" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Email</label>
                     <input type="text" id="email" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Nacionality</label>
                     <input type="text" id="nacionality" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>City</label>
                     <input type="text" id="city" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Province</label>
                     <input type="text" id="province" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Country</label>
                     <input type="text" id="country" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Native Language</label>
                     <input type="text" id="nativeLanguage" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Languages For Exchange</label>
                     <input type="text" id="languagesForExchange" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Spoken Languages</label>
                     <input type="text" id="spokenLanguages" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Phone Number</label>
                     <input type="text" id="phoneNumber" class="form-control" />
                  </div>
                  <button class="btn btn-primary" id="submit">Create</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
window.addEventListener('load',()=>{
  $("#submit").on("click",()=>{
      const member = {}
      member.name = $("#name").val()
      member.lname = $("#lname").val()
      member.username = $("#username").val()
      member.password = $("#password").val()
      member.bDay = $("#bDay").val()
      member.sex = $("#sex").val()
      member.email = $("#email").val()
      member.nacionality = $("#nacionality").val()
      member.city = $("#city").val()
      member.province = $("#province").val()
      member.country = $("#country").val()
      member.nativeLanguage = $("#nativeLanguage").val()
      member.phoneNumber = $("#phoneNumber").val()
      member.languagesForExchange = $("#languagesForExchange").val().split(",")
      member.spokenLanguages = $("#spokenLanguages").val().split(",")
      axios.post('/member',member)
        .then((resp) => {
          console.log('SUCCESS')
          console.log(resp.data)
        })
        .catch((err)=>{
          console.error(err.response.data)
        })
    })
})
</script>
@endsection
