<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>WeAllSpeak</title>
    <!-- Socket IO --> 
    <!-- <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script> -->
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        WeAll<span style="color:red;">Speak</span>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        @guest
                        <li>
                          <a href="{{ route('login') }}" class="nav-icon-link">
                            <span class="fa fa-sign-in" aria-hidden="true"></span>
                            <label>Login</label>
                          </a>
                        </li>
                        <li>
                          <a href="{{ route('register') }}" class="nav-icon-link">
                            <span class="fa fa-user-plus" aria-hidden="true"></span>
                            <label>Register</label>
                          </a>
                        </li>
                        @else

                          <li>
                            <a href="#" class="nav-icon-link">
                              <span class="fa fa-users" aria-hidden="true"></span>
                              <label>We</label>
                            </a>
                          </li>
                          <li>
                            <a href="#" class="nav-icon-link">
                              <span class="fa fa-search" aria-hidden="true"></span>
                              <label>All</label>
                            </a>
                          </li>
                          <li>
                            <a href="#" class="nav-icon-link">
                              <span class="fa fa-comments-o" aria-hidden="true"></span>
                              <label>Speak</label>
                            </a>
                          </li>
                          <li class="dropdown">
                              <a href="#" class="dropdown-toggle nav-icon-link" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <img class="img-thumbnail img-circle" src="{{asset(Auth::user()->image_url()) }}"  />
                              </a>

                              <ul class="dropdown-menu" role="menu">
                                  <li>                                  
                                	<a href="#" onclick="event.preventDefault(); document.getElementById('profile-form-home').submit();">
								        Home
							        </a>
                                

                                      <form id="profile-form-home" action="/dashboard" method="POST" style="display: none;">
                                          {{ csrf_field() }}
                                      </form>
                                  </li>                                   
                                  <li>
                                      
                                      <a href="#" onclick="event.preventDefault(); document.getElementById('profile-form-{{Auth::user()->username}}').submit();">
                                          Profile
                                        </a>
                                        
                                        
                                        <form id="profile-form-{{Auth::user()->username}}" action="/profile/{{Auth::user()->username}}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                   <li>
                                       <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                           Logout
                                       </a>
                                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           {{ csrf_field() }}
                                       </form>
                                   </li>
                                 
                              </ul>
                          </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
        @yield('profile')        
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
        @yield('scripts')

</body>
</html>
