@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-3">
          <div class="panel panel-default user-panel">
              <div class="panel-heading">
                <img class="img-thumbnail img-circle" src="{{ Auth::user()->image_url() }}"  />
              </div>
              <div class="panel-body">
                <label class="user-name">{{ Auth::user()->name}}</label>
                <dt>
                  <dl>
                    Friends
                  </dl>
                  <dd>
                    {{ Auth::user()->contacts()->count() }}
                  </dd>
                </dt>
              </div>
          </div>
      </div>
      <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                        <div class="form-group comment">
                                <label for="comment">Comment:</label>
                                <textarea id ="post-message"class="form-control"  rows="4" id="comment" style="resize:none"></textarea>
                                <button id = "send-message" type="button" class="btn btn-primary"style="float:right ; background-color: #009688; margin-top: 10px;">Publish</button>
                        </div>                
                </div>
                <div class="progress" style="height: 1px;">
                     <div class="progress-bar" role="progressbar" style="width: 25%; background-color: #90CAF9" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">My Messages</div>    
                <div class="panel-body form-group messages">
                    @forelse ($messages as $message)
                          <div class="chatContainer">
                            <img class="img-thumbnail img-circle" src="{{$message->path}}"  />
                            <p>{{$message->content}}</p>
                            <span class="time-right">{{$message->created_at}}</span>
                          </div>
                     @empty
                             <li>No messages yet</li>
                     @endforelse
              </div>
          </div>


      </div>
      <div class="col-md-3">
          <div class="panel panel-default">
              <div class="panel-heading">Contacts</div>

              <div class="panel-body">
                  <a href="#" id="add-contact-modal">                    
                    Add Contact
							    </a>
                <ul class="list-group">
                @forelse (Auth::user()->contacts as $contact)
                    <li class="list-group-item">
                      <img class="profile_pic sm"
                        src="{{ $contact->member->image_url()}}"  />
                      {{ $contact->member->name }}
                    </li>
                @empty
                    <li>No contacts yet</li>
                @endforelse
                </ul>
              </div>
          </div>
      </div>

    </div>
</div>


<div class="modal fade" id="EnSureModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Contact </h4>
          </div>
            <form action="/addContact" method="POST" id="add-contact-form">
              <div class="modal-body">
                <p>Enter email from the person</p>     
                  {{ csrf_field() }}
                  <div class="col-md-6">
                    <input id="add-contact-input" type="text" class="form-control" name="contact-email" value="searchPerson" required autofocus>
                  </div>           
              </div>
                <div class="modal-footer">
                  <button type="submit" id= "add-contact" class="btn btn-success" data-dismiss="modal"><i class="glyphicon glyphicon-ok"></i> Add Contact</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')

  
    <script type="text/javascript">

        function sendMessage(){
            console.log( document.getElementById('post-message').value);
                $.ajax({
                        type:'POST',
                        url:"/sendMessaje",
                        data:{
                            "_token": "{{ csrf_token() }}",
                            'name': "{{ Auth::user()->name}}}",
                            'data': document.getElementById('post-message').value,
                        },
                        dataType: 'json',
                        success:function(data){
                            console.log(data.status);
                        },
                        error:function(){

                        },
                });                
        }


        function addContact(){
        
          document.getElementById("add-contact-form").submit();

        }
        function showContactModal(){          
          $('#add-contact-modal').on( "click", function() {
            $("#EnSureModal").modal();            
          });
        }        
        document.getElementById('send-message').addEventListener('click', sendMessage)
        document.getElementById('add-contact-modal').addEventListener('click', showContactModal)
        document.getElementById('add-contact').addEventListener('click', addContact)

    Echo.private('ChatMessages').listen('MessageEvent', (e) => {
        console.log(e);
    });



    </script>

@endsection
