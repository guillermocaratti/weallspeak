@extends('layouts.app')

@section('content')

<!--  Error handle -->
        @if($errors->any())
        <div class="row collapse">
            <ul class="alert-box warning radius">
                @foreach($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
        @endif

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    
                    <form class="form-horizontal" method="POST" action="/register">
                        {{ csrf_field() }}
                        
                        <!-- <div style="width: 250px; margin-left: 30%;">
                        </div> -->
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('userName') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">User Name</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                            <label for="sex" class="col-md-4 control-label">Sex</label>

                            <div class="col-md-6">
                                <select id="sex" type="select" class="form-control" name="sex" required>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>

                                @if ($errors->has('sex'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sex') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       <div class="form-group{{ $errors->has('bDay') ? ' has-error' : '' }}">
                            <label for="bDay" class="col-md-4 control-label">Birthday</label>

                            <div class="col-md-6">
                                <input id="bDay" type="date" class="form-control" name="bDay" value="{{ old('bDay') }}" required>

                                @if ($errors->has('bDay'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bDay') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('nacionality') ? ' has-error' : '' }}">
                            <label for="nacionality" class="col-md-4 control-label">Country</label>

                            <div class="col-md-6">
                                <select id="nacionality" type="select" class="form-control" name="nacionality" required></select>

                                @if ($errors->has('nacionality'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nacionality') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('region') ? ' has-error' : '' }}">
                            <label for="region" class="col-md-4 control-label">Region</label>

                            <div class="col-md-6">
                                 <input id="region" type="text" class="form-control" name="region" value="{{ old('region') }}" required autofocus>

                                @if ($errors->has('region'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('region') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-md-4 control-label">City</label>

                            <div class="col-md-6">
                                <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" required autofocus>

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('native-language') ? ' has-error' : '' }}">
                            <label for="native-language" class="col-md-4 control-label">Native Language</label>

                            <div class="col-md-6">
                                <select id="native-language" type="select" class="form-control" name="native-language" required></select>

                                @if ($errors->has('native-language'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('native-language') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('spoken-languages') ? ' has-error' : '' }}">
                            <label for="spoken-languagese" class="col-md-4 control-label">Spoken Languages</label>

                            <div class="col-md-6">
                                <select id="spoken-languages" type="select" class="form-control" name="spoken-languages[]" multiple required></select>

                                @if ($errors->has('spoken-languages'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('spoken-languages') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('desired-languages') ? ' has-error' : '' }}">
                            <label for="desired-languages" class="col-md-4 control-label">Desired Languages</label>

                            <div class="col-md-6">
                                <select id="desired-languages" type="select" class="form-control" name="desired-languages[]" multiple required></select>

                                @if ($errors->has('desired-languages'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desired-languages') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Register
                            </button>
                        </div>
                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')

    <script type="text/javascript">

        function fillLanguage(selectInput){
            fetch('https://restcountries.eu/rest/v2/all')
                .then(function(res) {
                    return res.json();
                }).then(function(json) {
                    let result = json.map(function (prop) {
                        return prop.languages.map(function(prop){

                            return prop.name;
                        });
                    });
                        let arr = new Array();
                        let nativeLanguage = document.getElementById(selectInput);
                        result.forEach((languages) => {
                            // nativeLanguage.add(language);
                            languages.forEach((language) => {
                                
                                arr.push(language);
                                
                            });
                        });
                        
                        let allLanguages = removeDuplicates(arr);
                        allLanguages.forEach((l) => {
                            let option = document.createElement("option");                                
                            option.text = l;
                            option.value = l;
                        nativeLanguage.add(option);
                        })                                                        
                });
        }

        function allCodes(){
            fetch('https://restcountries.eu/rest/v2/all')
                .then(function(res) {
                    return res.json();
                }).then(function(json) {
                    let result = json.map(function (prop) {
                        return prop.languages.map(function(prop){

                            return prop.iso639_1;
                        });
                    });
                        let arr = new Array();
                        result.forEach((languages) => {
                            // nativeLanguage.add(language);
                            languages.forEach((language) => {
                                
                                arr.push(language);
                                
                            });
                        });
                        
                        let allLanguages = removeDuplicates(arr);
                        })                                                        
                };

        function allCountries(selectInput){
            fetch('https://restcountries.eu/rest/v2/all')
                .then(function(res) {
                    return res.json();
                }).then(function(json) {
                    let result = json.map(function (prop) {
                        return prop.name;
                    });
                        let arr = new Array();
                        let nativeLanguage = document.getElementById(selectInput);
                        result.forEach((country) => {
                             arr.push(country);
                        });
                        
                        let allLanguages = removeDuplicates(arr);
                        allLanguages.forEach((l) => {
                            let option = document.createElement("option");                                
                            option.text = l;
                            option.value = l;
                        nativeLanguage.add(option);
                        })                                                        
                });
        }
        function removeDuplicates(a){
            let hash = {};
            let out = [];
            out = a.filter(function(item, pos) {
                return a.indexOf(item) == pos;
            })
            return out
        }

        document.getElementById("nacionality").addEventListener("load", allCountries("nacionality"))
        document.getElementById("native-language").addEventListener("load", fillLanguage("native-language"))
        document.getElementById("spoken-languages").addEventListener("load", fillLanguage("spoken-languages"))
        document.getElementById("desired-languages").addEventListener("load", fillLanguage("desired-languages"))

    </script>

@endsection
