@extends('layouts.app')

@section('profile')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default user-panel">
              <div class="panel-heading">
                <button id ="profile-image"  style="border:0; background:transparent">
                  @if(Auth::user()->image)
                      <img class="img-thumbnail img-circle" src="{{asset(Auth::user()->image->path) }}"  />
                  @else
                      <img class="img-thumbnail img-circle" src="{{ Auth::user()->picturePath}}"  />
                  @endif                
                </button>

              </div>


              <div class="panel-body">
                <form class="form-horizontal" method="POST" action="/edit">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            
                        
                        <label class="user-name">{{ Auth::user()->name}}</label>


                        <label for="name" class="col-md-4 control-label">User Name</label>
                        <div class="col-md-6">
                           <input id="username" type="text" class="form-control" name="username" value="{{ Auth::user()->username }}" required autofocus>
                        </div>

                         <label for="sex" class="col-md-4 control-label">Sex</label>
                        <div class="col-md-6">
                            <select id="sex" type="select" class="form-control" name="sex" required>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>
                        <label for="bDay" class="col-md-4 control-label">Birthday</label>
                        <div class="col-md-6"> 
                            <input id="bDay" type="date" class="form-control" name="bDay" value="{{  Auth::user()->bDay }}" required>
                        </div>
                        <label class="col-md-4 control-label" >Country</label>
                        <div class="col-md-6">
                            <input id="user-country-input" type="text" class="form-control" name="region" value="{{ Auth::user()->nacionality}}" required autofocus>
                        </div>
                        <label class="col-md-4 control-label" >City</label>
                        <div class="col-md-6">
                            <input id="user-city-input" type="text" class="form-control" name="gender" value="{{ Auth::user()->city}}" required autofocus>
                        </div>                                            
                        <label class="col-md-4 control-label" >Province</label>
                        <div class="col-md-6">
                            <input id="user-province-input" type="text" class="form-control" name="gender" value="{{ Auth::user()->province}}" required autofocus>
                        </div>            
                        <label class="col-md-4 control-label" >Phone Number</label>
                        <div class="col-md-6">
                            <input id="user-p-input" type="text" class="form-control" name="gender" value="{{ Auth::user()->phoneNumber}}" required autofocus>
                        </div>     
                        <label class="col-md-4 control-label">Native Language</label>
                        <div class="col-md-6">
                             <select id="native-language" type="select" class="form-control" name="native-language" required></select>
                        </div>     
                        <label class="col-md-4 control-label">Spoken Language</label>
                        <div class="col-md-6">
                              <select id="spoken-languages" type="select" class="form-control" name="spoken-languages[]" multiple required></select>
                        </div>     
                        <label class="col-md-4 control-label">Desired Language</label>
                        <div class="col-md-6">
                              <select id="desired-languages" type="select" class="form-control" name="desired-languages[]" multiple required></select>
                        </div>     


                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Edit
                            </button>
                        </div>
                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="upload-img-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Profile Picture </h4>
          </div>
            <form action="/storeImage" method="POST" id="upload-img-form" enctype="multipart/form-data">
              <div class="modal-body">
                <p>Upload your profile photo</p>     
                  {{ csrf_field() }}
	            <div class="form-group">
	            	<div class="input-group input-file" name="profilePictureInput">
	            		<span class="input-group-btn">
                    		<button class="btn btn-default btn-choose" type="button">Choose</button>
                		</span>
                		<input type="text" class="form-control" name = "profilePicture" placeholder='Choose a file...' />
                		<span class="input-group-btn">
                   			 <button class="btn btn-warning btn-reset" type="button">Reset</button>
                		</span>
	            	</div>
	            </div>        
              </div>
                <div class="modal-footer">
                  <button type="submit" id= "img-upload" class="btn btn-success" data-dismiss="modal"><i class="glyphicon glyphicon-ok"></i> Change Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
 
    <script type="text/javascript">

        function uploadImage(){
        
          document.getElementById("upload-img-form").submit();

        }
        function showUploadModal(){          
          $('#profile-image').on( "click", function() {
            $("#upload-img-modal").modal();       
            bs_input_file();     
          });
        }        
        document.getElementById('profile-image').addEventListener('click', showUploadModal)
        document.getElementById('img-upload').addEventListener('click', uploadImage)


        function bs_input_file() {
    	$(".input-file").before(
    		function() {
    			if ( ! $(this).prev().hasClass('input-ghost') ) {
    				var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
    				element.attr("name",$(this).attr("name"));
    				element.change(function(){
    					element.next(element).find('input').val((element.val()).split('\\').pop());
    				});
    				$(this).find("button.btn-choose").click(function(){
    					element.click();
    				});
    				$(this).find("button.btn-reset").click(function(){
    					element.val(null);
    					$(this).parents(".input-file").find('input').val('');
    				});
    				$(this).find('input').css("cursor","pointer");
    				$(this).find('input').mousedown(function() {
    					$(this).parents('.input-file').prev().click();
    					return false;
    				});
    				return element;
    			}
    		}
    	);
    }



        function fillLanguage(selectInput){
            fetch('https://restcountries.eu/rest/v2/all')
                .then(function(res) {
                    return res.json();
                }).then(function(json) {
                    let result = json.map(function (prop) {
                        return prop.languages.map(function(prop){

                            return prop.name;
                        });
                    });
                        let arr = new Array();
                        let nativeLanguage = document.getElementById(selectInput);
                        result.forEach((languages) => {
                            // nativeLanguage.add(language);
                            languages.forEach((language) => {
                                
                                arr.push(language);
                                
                            });
                        });
                        
                        let allLanguages = removeDuplicates(arr);
                        allLanguages.forEach((l) => {
                            let option = document.createElement("option");                                
                            option.text = l;
                            option.value = l;
                        nativeLanguage.add(option);
                        })                                                        
                });
        }

        function getNativeLanguage(){

            $.ajax({
                url: "/profile/getNativeLanguage",
                type: 'GET',
                success: function(result){
                    var lng = JSON.parse(result);
                    console.log("lenguaje nativos " + lng.country);
                    document.getElementById("native-language").value = lng.country;
                }
            });          
        }
        function getDesiredLanguage(){

            $.ajax({
                url: "/profile/getDesiredLanguage",
                type: 'GET',
                success: function(result){
                    var lng = JSON.parse(result);
                    lng.forEach((ln) => {
                        console.log(ln.country);

                         document.getElementById("desired-languages").value = ln.country;
                    })
                    
                }
            });          
        }
        
        function getSpokenLanguage(){

            $.ajax({
                url: "/profile/getSpokenLanguage",
                type: 'GET',
                success: function(result){
                    var lng = JSON.parse(result);
                    console.log(lng);
                    lng.forEach((ln) => {
                        console.log(ln.country);
                        document.getElementById("spoken-languages").value = ln.country;
                        document.getElementById("spoken-languages").selected = true
                      
                    })
                }
            });          
        }


        function allCodes(){
            fetch('https://restcountries.eu/rest/v2/all')
                .then(function(res) {
                    return res.json();
                }).then(function(json) {
                    let result = json.map(function (prop) {
                        return prop.languages.map(function(prop){

                            return prop.iso639_1;
                        });
                    });
                        let arr = new Array();
                        result.forEach((languages) => {
                            // nativeLanguage.add(language);
                            languages.forEach((language) => {
                                
                                arr.push(language);
                                
                            });
                        });
                        
                        let allLanguages = removeDuplicates(arr);
                        })                                                        
                };

        function allCountries(selectInput){
            fetch('https://restcountries.eu/rest/v2/all')
                .then(function(res) {
                    return res.json();
                }).then(function(json) {
                    let result = json.map(function (prop) {
                        return prop.name;
                    });
                        let arr = new Array();
                        let nativeLanguage = document.getElementById(selectInput);
                        result.forEach((country) => {
                             arr.push(country);
                        });
                        
                        let allLanguages = removeDuplicates(arr);
                        allLanguages.forEach((l) => {
                            let option = document.createElement("option");                                
                            option.text = l;
                            option.value = l;
                        nativeLanguage.add(option);
                        })                                                        
                });
        }
        function removeDuplicates(a){
            let hash = {};
            let out = [];
            out = a.filter(function(item, pos) {
                return a.indexOf(item) == pos;
            })
            return out
        }
        // document.getElementById("nacionality").addEventListener("load", allCountries("nacionality"))
        document.getElementById("native-language").addEventListener("load", fillLanguage("native-language"))
        document.getElementById("spoken-languages").addEventListener("load", fillLanguage("spoken-languages"))
        document.getElementById("desired-languages").addEventListener("load", fillLanguage("desired-languages"))
        getNativeLanguage();
        getDesiredLanguage();
        getSpokenLanguage();

    </script>

@endsection