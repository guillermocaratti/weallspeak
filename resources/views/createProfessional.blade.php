@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div>
                  <div class="form-group" >
                     <label>Name</label>
                     <input type="text" id="name" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Last Name</label>
                     <input type="text" id="lname" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Email</label>
                     <input type="text" id="email" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>City</label>
                     <input type="text" id="city" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Province</label>
                     <input type="text" id="province" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Country</label>
                     <input type="text" id="country" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Native Language</label>
                     <input type="text" id="nativeLanguage" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Languages</label>
                     <input type="text" id="languages" class="form-control" />
                  </div>
                  <div class="form-group" >
                     <label>Phone Number</label>
                     <input type="text" id="phoneNumber" class="form-control" />
                  </div>
                  <button class="btn btn-primary" id="submit">Create</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
window.addEventListener('load',()=>{
  $("#submit").on("click",()=>{
      const prof = {}
      prof.name = $("#name").val()
      prof.lname = $("#lname").val()
      prof.email = $("#email").val()
      prof.city = $("#city").val()
      prof.province = $("#province").val()
      prof.country = $("#country").val()
      prof.nativeLanguage = $("#nativeLanguage").val()
      prof.phoneNumber = $("#phoneNumber").val()
      prof.languages = $("#languages").val().split(",")
      axios.post('/profesor',prof)
        .then((resp) => {
          console.log('SUCCESS')
          console.log(resp.data)
        })
        .catch((err)=>{
          console.error(err.response.data)
        })
    })
})
</script>
@endsection
