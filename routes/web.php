<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Events\MessageEvent;

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profesor/create',function (){
  return view('createProfessional');
});


Route::post('/dashboard', 'HomeController@index')->name('home');
Route::post('/profesor','ProfesorController@create');
Route::get('/profesor','ProfesorController@all');
Route::get('/profesor/{id}','ProfesorController@byId');
Route::post('/profile/{username}','ProfileController@show')->name('profile');

Route::post('/addContact','ContactController@create');

Route::get('/image/{hash}','ImageController@view');

Route::post('/edit','ProfileController@edit');
Route::post('/storeImage','ProfileController@storeImage');

Route::get('event',function(){
  event(new MessageEvent("Mi primer mensaje emitido"));
});

Route::post('/sendMessaje', 'MessajeController@postMessaje')->middleware('auth');;

Route::get('/profile/getNativeLanguage', 'ProfileController@getNativeLanguage')->name('home');
Route::get('/profile/getDesiredLanguage', 'ProfileController@getDesiredLanguage')->name('home');
Route::get('/profile/getSpokenLanguage', 'ProfileController@getSpokenLanguage')->name('home');

Auth::routes();
