<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $fillable = ['name', 'email', 'password','username','nacionality','bDay','sex','country','province','city','native_language_id','spoken_languages','desired_languages'];
    
    public function languagesForExchange(){
        return $this->belongsToMany('App\Language','member_languagesForExchange');
    }

    public function spokenLanguages(){
        return $this->belongsToMany('App\Language','member_spokenLanguages');
    }

    public function nativeLanguage(){
        return $this->belongsTo('App\Language');
    }

    public function contacts(){
        return $this->hasMany('App\Contact');
    }

    public function image_url(){
        return "image/".$this->image_hash;
    }
    
}
