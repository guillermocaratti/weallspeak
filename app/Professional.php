<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professional extends Model
{
  public function languages()
  {
      return $this->belongsToMany('App\Language');
  }

  public function nativeLanguage()
  {
      return $this->belongsTo('App\Language','nativeLanguage_id');
  }

}
