<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public function member(){
      return $this->belongsTo("App\User","to_user_id");
    }
}
