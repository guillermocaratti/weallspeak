<?php

namespace App\Http\Controllers;

use App\User;
use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ContactController extends Controller
{
 

    public function create(Request $request)
    {
        $email = $request->input('contact-email');

        $userToAdd = User::where("email",$email)->get();
        $idToAdd = $userToAdd[0]->id;
        
        $userId = Auth::user()->id;

        $contact = new Contact();

        $contact->user_id = $userId;
        $contact->to_user_id = $idToAdd;
        $contact->relation = 0;
        $contact->save();
        return app('App\Http\Controllers\HomeController')->index();
    }


}
