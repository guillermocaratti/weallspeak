<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $id = Auth::user()->id;
        $messages = Chat::where("to_user_id",$id)
            ->orWhere("chats.user_id",$id)       
            ->get();                
        return view('home',[ "messages" => $messages ]);

    }
}
