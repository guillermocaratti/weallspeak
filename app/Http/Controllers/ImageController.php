<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    function view (string $hash){
        return Storage::download("public/$hash.jpg");
    }
}
