<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Language;
use App\SpokenLanguages;
use App\DesiredLanguages;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     * 
     */
    protected function validator(array $data)
    {

        $result = Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
            'native-language.*' => 'required|string|max:255',
            'spoken-languages.*' => 'required|string|max:255',
            'desired-languages.*' => 'required|string|max:255',
            ]);
        
        return $result;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {


        $native_language_id = Language::where('native',$data['native-language'])->get();

        $spoken_id_arr = [];
        foreach ($data['spoken-languages'] as $spoken) {
            $value = Language::where('native',$spoken)->get();
            foreach ($value as $v) {
                array_push($spoken_id_arr,$v->id);
            }
        }

        $desiered_id_arr = [];
        foreach ($data['desired-languages'] as $desiered) {
            $value = Language::where('native',$desiered)->get();
            foreach ($value as $v) {            
                array_push($desiered_id_arr,$v->id);
            }
        }
        

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'nacionality' => $data['nacionality'],
            'bDay' => $data['bDay'],
            'sex' => $data['sex'],
            'country'=> $data['nacionality'],                        
            'city'=> $data['city'],
            'province'=> $data['region'],
            'native_language_id' => $native_language_id[0]->id,
            'spoken_languages' => json_encode($spoken_id_arr,true),
            'desired_languages' => json_encode($desiered_id_arr,true)
        ]);
        
        
        foreach ($spoken_id_arr as $value) {
            $language = new SpokenLanguages();
            $language->user_id = $user->id;
            $language->language_id = $value;
            $language->save();
        }

        foreach ($desiered_id_arr as $value) {
            $language = new DesiredLanguages();
            $language->user_id = $user->id;
            $language->language_id = $value;
            $language->save();
        }

        return $user;
    }
}