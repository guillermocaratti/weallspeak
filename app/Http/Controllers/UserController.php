<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Language;
use App\User;

use Carbon\Carbon;

use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
  public function create(Request $req){
    $req->validate([
      "name" => "required|string|max:255|",
      "lname" => "required|string|max:255|",
      "username" => "required|string|max:50|unique",
      "email" => "required|string|email|max:255|unique",
      "bDay" => "required|string",
      "nacionality" => "required|string|max:60|exist:",
      "sex" => "required|string|max:25",
      "city" => "required|string|max:255|",
      "province" => "required|string|max:255|",
      "country" => "required|string|max:60|",
      "languagesForExchange"=>"required|array|min:1|",
      "spokenLanguages"=>"required|array|min:1|",
      "nativeLanguage" => "required|string|max:70|",
      "phoneNumber"=> "string|max:50|",
      "password" => "required|string"
      #must contains one digit from 0-9, one lowercase characters,
      #one uppercase characters,length at least 6 characters and maximum of 20
    ]);

    $m = new User;
    $m->city = $req->province;
    $m->province = $req->province;
    $m->country = $req->country;
    $nl = Language::where('code',$req->nativeLanguage)
      ->firstOrFail();
    $m->nativeLanguage()->associate($nl);
    $m->phoneNumber= $req->phoneNumber;
    $m->availableHours=$req->availableHours;
    $m->bDay = Carbon::parse($req->bDay);
    $m->nacionality = $req->nacionality;
    $m->sex= $req->sex;


    $langsIds = DB::table('languages')
                    ->whereIn('code',$req->languagesForExchange)
                    ->pluck('id');
    $m->languagesForExchange()->attach($langsIds);

    $langsIds = DB::table('languages')
                    ->whereIn('code',$req->spokenLanguages)
                    ->pluck('id');
    $m->spokenLanguages()->attach($langsIds);


    $m->name = $req->name;
    $m->lname= $req->lname;
    $m->email = $req->email;
    $m->password = bycript($req->password);
    $m->username = $req->username;
    $m->save();

    return "ok";
  }

}
