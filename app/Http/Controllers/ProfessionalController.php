<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Professional;
use App\Language;
use Illuminate\Support\Facades\DB;


abstract class ProfessionalController extends Controller
{
  public function create(Request $req){
    $req->validate([
      "name" => "required|string|max:255|",
      "lname" => "required|string|max:255|",
      "email" => "required|string|email|max:255|",
      "city" => "required|string|max:255|",
      "province" => "required|string|max:255|",
      "country" => "required|string|max:60|",
      "languages"=>"required|array|min:1|",
      "nativeLanguage" => "required|string|max:70|",
      "phoneNumber"=> "string|max:50|"
    ]);

    $p = new Professional;
    $p->name = $req->name;
    $p->lname= $req->lname;
    $p->type = $this->type;
    $p->email = $req->email;
    $p->city = $req->province;
    $p->province = $req->province;
    $p->country = $req->country;
    $nl = Language::where('code',$req->nativeLanguage)
      ->firstOrFail();
    $p->nativeLanguage()->associate($nl);
    $p->phoneNumber= $req->phoneNumber;
    $p->availableHours=$req->availableHours;
    $p->save();

    $langsIds = DB::table('languages')
                    ->whereIn('code',$req->languages)
                    ->pluck('id');
    $p->languages()->attach($langsIds);
    return "Ok";
  }

  public function all(){
    return Professional::where('type',$this->type)->get();
  }

  public function byId($id){
      return Professional::where('type',$this->type)
                          ->where('id',$id)
                          ->first();
  }

}
