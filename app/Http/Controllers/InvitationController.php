<?php

namespace App\Http\Controllers;

use App\Mail\InvitationMail;
use App\Invitation;
use App\User;
use Illuminate\Http\Request;

class InvitationController extends Controller
{
   public function make(Request $req){
     $req->validate([
       "name" => "required|string|max:255|",
       "lname" => "required|string|max:255|",
       "email" => "required|string|email|max:255|"
     ]);

     $email = $req->email;
     $lname = $req->lname;
     $name = $req->name;

     if(User::where('email',$email)
          ->count() > 0){
            return "this email is allready is registered.";
     }

     if(Invitation::where('email',$email)
          ->where('created_at','>',Carbon::yesterday())
          ->count() > 0){
            return "you allready send a invitation to that person";
     }

     if(Invitation::where('user',Auth::user())
          ->where('created_at','>',Carbon::yesterday())
          ->count() > 4){
            return "You can only send 5 invitations per day";
     }

     $to = [ "email" => $email , "name" => $name . ' ' .  $lname ];

     Mail::to($to)->send(new InvitationMail($email, $name,$lname));

     $invitation = new App\Invitation;
     $invitation->email = $req->email;
     $invitation->save();

   }
}
