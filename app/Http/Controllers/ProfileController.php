<?php

namespace App\Http\Controllers;

use App\User; 
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeImage(Request $request)
    {
        
        $id = Auth::user()->id;
        //si lo encuentro actualizo sus valores sino creo uno nuevo
        //solo una imagen por usuario
        $file = Image::where("user_id",$id)->first();
        
        if ($file) {
            // dd("actualizo");                
            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->profilePictureInput->getClientOriginalName());            
            $file->name = $withoutExt;
            $file->size = $request->profilePictureInput->getClientSize();
            $file->mime_type = $request->profilePictureInput->getClientMimeType();
            $file->path = '/storage/img/'. Storage::disk('public')->put('pf-img-'.$id,$request->file('profilePictureInput'));
            // $file->path = '/storage/public/' . $request->file('profilePictureInput')->store('pf-img-'.$id);
            $file->user()->associate(Auth::user());
            $file->hashName =  md5($withoutExt . microtime());
            
            $file->save();

        }else{
            // dd("creo");                
            $file = new Image();
            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->profilePictureInput->getClientOriginalName());
            $file->name = $withoutExt;
            $file->size = $request->profilePictureInput->getClientSize();
            $file->mime_type = $request->profilePictureInput->getClientMimeType();
            $file->path = '/storage/img/'. Storage::disk('public')->put('pf-img-'.$id,$request->file('profilePictureInput'));
            // $file->path = '/storage/public/' . $request->file('profilePictureInput')->store('pf-img-'.$id);
            $file->user()->associate(Auth::user());
            $file->hashName =  md5($withoutExt . microtime());            
            
            $file->save();            
        }
            
            $user =  Auth::user();
            $user->picturePath = $file->id;
            $user->save();
            
        return app('App\Http\Controllers\ProfileController')->show($user);
    }

    public function getNativeLanguage(Request $request)
    {         
        $countryToGet = Auth::user()->nativeLanguage()->get();
        $response = [];
        foreach ($countryToGet as $country) {
            $response["id"] = $country["id"];
            $response["code"] = $country["code"];
            $response["country"] = $country["native"];
        }
        return response()->json(json_encode($response));
    }

    public function getDesiredLanguage(Request $request)
    {         
        $countryToGet = Auth::user()->languagesForExchange()->get();
        $response = [];
        foreach ($countryToGet as $country) {
            $countryArr = [];
            $countryArr["id"] = $country["id"];
            $countryArr["code"] = $country["code"];
            $countryArr["country"] = $country["native"];
            array_push($response,$countryArr);
        }
        
        return response()->json(json_encode($response));
    }
    public function getSpokenLanguage(Request $request)
    {         
        $countryToGet = Auth::user()->spokenLanguages()->get();
        $response = [];
        foreach ($countryToGet as $country) {
            $countryArr = [];
            $countryArr["id"] = $country["id"];
            $countryArr["code"] = $country["code"];
            $countryArr["country"] = $country["native"];
            array_push($response,$countryArr);
        }
        return response()->json(json_encode($response));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
      return view("profile");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        dd($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
